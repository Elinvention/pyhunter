#!/usr/bin/python3

import pygame
from pygame.locals import *
import os
import sys
import random


VERSION="0.0-dev"
SCREEN_W = 800
SCREEN_H = 600
YELLOW = (255, 255, 0) #fiero
WHITE = (255,255,255)

pygame.init()
#pygame.display.set_icon(pygame.image.load(os.path.join('images', 'icon.png')))
screen = pygame.display.set_mode((SCREEN_W, SCREEN_H))
pygame.display.set_caption("PyHunter " + VERSION)
clock = pygame.time.Clock()

font = pygame.font.SysFont("Xolonium", 12)
snake = pygame.image.load('snake.png')
player_sprite = pygame.image.load('player.png')
bullet_image = pygame.Surface((6, 6)).convert_alpha()
bullet_image.fill((0,0,0,0)) # transparent
pygame.draw.circle(bullet_image, YELLOW, (3, 3), 3)
enemy_splat = pygame.mixer.Sound('impactsplat04.ogg')
blood_img = pygame.image.load('blood.png')


def blit_alpha(target, source, location, opacity):
    temp = pygame.Surface(source.get_size()).convert()
    temp.blit(target, (-location[0], -location[1]))
    temp.blit(source, (0, 0))
    temp.set_alpha(opacity)
    target.blit(temp, location)


class Player(object):
    SIZE = (45, 45)
    def __init__(self, image, pos, weapon, accel):
        self.image = image
        self.rect = pygame.Rect(pos, self.SIZE)
        self.weapon = weapon
        self.accel = accel
        self.x_speed = self.y_speed = 0
        self.money = 0
        self.shots = 0
        self.hit = 0
        self.moving = False

    def update(self, up, right, down, left):
        if not up and not right and not down and not left:
            self.moving = False
        else:
            self.moving = True

        if up:
            self.y_speed -= self.accel
        if down:
            self.y_speed += self.accel
        if left:
            self.x_speed -= self.accel
        if right:
            self.x_speed += self.accel

        if not up and not down:
            if self.y_speed > 0:
                self.y_speed -= self.accel/4
            elif self.y_speed < 0:
                self.y_speed += self.accel/4
        if not left and not right:
            if self.x_speed > 0:
                self.x_speed -= self.accel/4
            elif self.x_speed < 0:
                self.x_speed += self.accel/4

        self.rect.move_ip(self.x_speed, self.y_speed)

        if self.rect.top < 0:
            self.rect.top = 0
            self.y_speed = 0
        elif self.rect.bottom > SCREEN_H:
            self.rect.bottom = SCREEN_H
            self.y_speed = 0
        if self.rect.left < 0:
            self.rect.left = 0
            self.x_speed = 0
        elif self.rect.right > SCREEN_W:
            self.rect.right = SCREEN_W
            self.x_speed = 0

        if self.weapon.bullets <= 0:
           self.weapon.reload()

    def fire(self):
        if self.weapon.fire():
            self.shots += 1
            self.x_speed -= self.weapon.backlash
            return Bullet(bullet_image, self.rect.midright, (30 + self.x_speed, self.y_speed))

    def draw(self, screen):
        t = pygame.time.get_ticks() - self.weapon.last_shot
        frame = t // 250 % 4
        if self.moving:
            y = 45
        else:
            y = 0
        if t < 1000: # shooting animation
            x = 180
        else:
            x = 0
        offset = (x + frame * self.SIZE[0], y)
        screen.blit(self.image, self.rect, pygame.Rect(offset, self.SIZE))

        if 0 < self.weapon.reloading < 100:
            txt = font.render("%d%%" % self.weapon.reloading, True, WHITE)
            screen.blit(txt, self.rect.move(0, -txt.get_height() if self.rect.top > txt.get_height() else self.rect.height))

class Weapon(object):
    def __init__(self, name, fire_snd, rel_snd, dmg, cps, max_bullets, reload_speed, backlash, price):
        self.name = name
        self.image = pygame.image.load("%s.png" % name)
        self.fire_snd = pygame.mixer.Sound(fire_snd)
        self.rel_snd = pygame.mixer.Sound(rel_snd)
        self.ch = pygame.mixer.Channel(1)
        self.dmg = dmg
        self.cps = cps
        self.price = price
        self.bullets = self.max_bullets = max_bullets
        self.reload_speed = reload_speed
        self.backlash = backlash
        self.reloading = 0
        self.last_shot = -1000

    def fire(self):
        if self.bullets > 0:
            if self.last_shot + 1000 // self.cps < pygame.time.get_ticks():
                self.last_shot = pygame.time.get_ticks()
                self.fire_snd.play()
                self.bullets -= 1
                return True
        return False

    def reload(self):
        self.bullets = 0
        if self.reloading >= 100:
            self.bullets = self.max_bullets
            self.reloading = 0
        else:
            self.reloading += self.reload_speed
            if not self.ch.get_busy():
                self.ch.play(self.rel_snd)

class Bullet(object):
    def __init__(self, image, pos, speed):
        self.image = image
        self.rect = pygame.Rect(pos, self.image.get_size())
        self.speed = speed

    def draw(self, screen):
        screen.blit(self.image, self.rect)

    def update(self):
        self.rect.move_ip(*self.speed)

class Enemy(object):
    SIZE = (32, 32)
    def __init__(self, image, sound, hp, speed, value):
        self.image = image
        self.rect = pygame.Rect((SCREEN_W - 1, random.randint(0, SCREEN_H - self.SIZE[1])), self.SIZE)
        self.speed = speed
        self.hp = hp
        self.sound = sound
        self.born = pygame.time.get_ticks()
        self.value = value
        print(self.__repr__())

    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.hp <= 0:
            self.sound.play()

    def draw(self, screen):
        t = pygame.time.get_ticks() - self.born
        screen.blit(self.image, self.rect, pygame.Rect((t // 250 % 10 * self.SIZE[0], 80), self.SIZE))

    def __repr__(self):
        return "<Enemy(%s, %d, %d %d)>" % (self.rect, self.speed, self.hp, self.value)

class Blood(object):
    def __init__(self, image, pos, value):
        self.rect = pygame.Rect(pos, image.get_size())
        self.image = image
        self.created = pygame.time.get_ticks()
        self.alpha = 255
        gain = font.render("%d€" % value, True, WHITE)
        self.image.blit(gain, gain.get_rect(center=(self.rect.width // 2, self.rect.width // 2)))

    def update(self):
        t = pygame.time.get_ticks() - self.created
        if t > 10000:
            self.alpha = 255 - (t - 10000) // 100

    def draw(self, screen):
        blit_alpha(screen, self.image, self.rect.topleft, self.alpha)

class Shop(object):
    def __init__(self, items):
        self.items = items

    def buy(self, player, item):
        player.money -= item.price
        self.items.remove(item)


pistol = Weapon("Pistol", "cz.ogg", "reload.ogg", 1, 2, 12, 0.5, 2, 100)
player = Player(player_sprite, (100, SCREEN_H//2), pistol, 0.2)

bullets = []
enemies = []
bloods = []
moveUp = moveDown = moveLeft = moveRight = fire = False
done = False

while not done:  # check for events
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN:  # change the keyboard variables
            if event.key == K_LEFT or event.key == K_a:
                moveRight = False
                moveLeft = True
            elif event.key == K_RIGHT or event.key == K_d:
                moveLeft = False
                moveRight = True
            elif event.key == K_UP or event.key == K_w:
                moveDown = False
                moveUp = True
            elif event.key == K_DOWN or event.key == K_s:
                moveUp = False
                moveDown = True
            elif event.key == K_SPACE:
                fire = True
        elif event.type == KEYUP:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
            elif event.key == K_LEFT or event.key == K_a:
                moveLeft = False
            elif event.key == K_RIGHT or event.key == K_d:
                moveRight = False
            elif event.key == K_UP or event.key == K_w:
                moveUp = False
            elif event.key == K_DOWN or event.key == K_s:
                moveDown = False
            elif event.key == K_SPACE:
                fire = False
            elif event.key == K_r:
                player.weapon.reload()

    if random.randint(0, 100) == 0:
        hp = int(1 + random.gauss(0.5, 1))
        speed = int(1 + random.gauss(0.5, 1))
        if hp > 0 and speed > 0:
            enemies.append(Enemy(snake, enemy_splat, hp, speed, 50 * speed + 50 * hp))

    player.update(moveUp, moveRight, moveDown, moveLeft)

    if fire:
        bullet = player.fire()
        if bullet:
            bullets.append(bullet)

    screen.fill((0,0,0))

    for blood in bloods:
        blood.update()
        if blood.alpha == 0:
            bloods.remove(blood)
        else:
            blood.draw(screen)

    player.draw(screen)

    for bullet in bullets:
        bullet.update()
        if not screen.get_rect().colliderect(bullet.rect):
            # bullet out of screen: let the garbage collector do it's work
            bullets.remove(bullet)
        else:
            for enemy in enemies:
                if bullet.rect.colliderect(enemy.rect):
                    bullets.remove(bullet)
                    enemy.hp -= player.weapon.dmg
                    player.hit += 1
                    break
            bullet.draw(screen)

    for enemy in enemies:
        enemy.update()
        if enemy.hp <= 0:
            bloods.append(Blood(blood_img.copy(), blood_img.get_rect(center=enemy.rect.center).topleft, enemy.value))
            player.money += enemy.value
            enemies.remove(enemy)
        else:
            enemy.draw(screen)
            if not screen.get_rect().colliderect(enemy.rect):
                print("Game Over!")
                print(enemy)
                txt = font.render("Game Over!", True, WHITE)
                screen.blit(txt, txt.get_rect(center=screen.get_rect().center).topleft)
                done = True

    stat = font.render("Money: %s€   Bullets: %s   Accuracy: %.2f%%   Current weapon: %s" % (player.money, player.weapon.bullets, player.hit/player.shots * 100 if player.shots > 0 else 0., player.weapon.name), True, WHITE)
    screen.blit(stat, stat.get_rect(bottom=SCREEN_H))

    pygame.display.update()
    clock.tick(60)

pygame.event.clear()

event = pygame.event.wait()
while not (event.type == KEYDOWN and event.key == K_RETURN) and event.type != QUIT:
    event = pygame.event.wait()
    clock.tick(60)

pygame.quit()
sys.exit()

